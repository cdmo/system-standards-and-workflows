#Project Development Workflow and Environment DRAFT *December 5, 2014*Some basic principles:* Do it the *The Drupal Way** Don’t hack core or contrib.** Overide, overide, overide
* Separate logic from theming
* Be leary of over-engineering
* DRY - Don't Repeat Yourself
* Code should be self-documenting
* Automate trivial, monotonous tasks
* Don't go rogue
* 100% code coverage
* As few modules as possible
* No appendages
* Don't touch the MySQL directly
*If you need to hack core/contrib, this should be reviewed and signed off on by others first. ##Coding StandardsFollow these guidelines on things like indentation, naming conventions, commenting style and more:[Drupal.org Coding Standards](https://www.drupal.org/coding-standards)[Coder Module](https://www.drupal.org/project/coder) can help with this too, think of it as a linter.
For those of us on SublimeText, here are some projects and settingsthat can help make coding easier:

[text here]

##Code Repository
We will use [PSU's GitLab](git.psu.edu) as our code repository.
Commits should be more than glorified save points.  They should be *discrete* or *atomic* additions or modifications of functionality.  Avoid aggregating a multitude of unrelated modifications. 
All commits should have brief messages that reflect the nature of the change.  Try to succinctly emulate what would be the explanation during a code review.[SourceTree](http://www.sourcetreeapp.com/) is a great GUI for Git that can make atomic commits trivially easy.
##Development Environments
Local >> Development >> QA >> ProductionWe'll all develop to the Cent OS VM that Eric Treece has established for us.  It will be a replica of what should be expected at the shared environments in our domain.  Local will be the place where we hack, play and polish code.  Here's how it'll work in terms of the code repo:
1. Checkout and create a branch `git checkout -b new_feature` in your local VM.
1. Get your code to the point where it's solving the problem.
2. Merge your code with the `dev` branch
3. Test
4. Merge code with the `qa` branch
5. Test
6. Merge code with the `production` branch
7. Monitor for any unforseen issues

##Project Management

###Scrums

Group meetings for no more than 10 minutes every day at 10:00am.  What are you working on, what have you done, what will you be doing, what problems exist.  The 10 minute meeting is not meant to be a place to resolve problems or go in-depth into anything, it is a mere temperature check.  Anything that needs further discussion can happen outside the meeting later with appropriate parties.###Issue Tracking
Use GitLab's inherent issue tracking system to track features and bugs.  These will be organized by Charlie into Milestones.  Relevant items of interest will be shared by Charlie into MS Project.
##Drupal System and Sub-Systems
Work-in-progress.  The following should be some questions we ask ourselves when we create something in Drupal.  The idea is to manage our content management system.  We don't want any appendages just hanging out taking up space, or worse, bottlenecking.###Modules
Modules need to have a reason to be enabled.  That reason should be justified to the group and documented.  That documentation should live in Box and be continuously updated.###Views
###Roles
###Blocks
###Content Types##File OrganizationPut code that is solely front-end oriented in the theme.Put code that is solely back-end oriented in a custom module.Modules should be organized by sub-system OR type.  Examples:Sub-system: All of the hooks, custom PHP, CSS, JavaScript, templating, etc. required to retrieve tech lending device availability data and display it to the end user.Type: There are many common hooks that get used per content-type, view, service, etc.  Things that you use to swoop in and tamper with before Drupal sends something to the user.  For the sake of sanity, it’s best to keep this all in one place.  Some examples: form alterations, service alterations, feed modifications, etc.If there’s custom code associated with a Feature, just put that code in the feature’s `.module` file.##Feature ModulesFeature modules should be used as much as possible for the sake of code history, performance and code safety (i.e., falling back to a working revision).  The Strongarm module must be used as well, which allows you to grab deeper items in Drupal and write them to code.Feature modules should be relatively small.  For example, everything that a given content type might require in order to deliver results to the user: the definition of the content type, associated views, dependent modules, image styles, etc.  Features that grow too large can become unwieldy.##Code ReviewsLet's try to create a culture where we are open to sharing our code with each other, and be open to new ways of doing things.  We are all human, we all make mistakes, none of us are perferct.
During a code review session the coder can talk about the story of the code: provide context, explain why the code is needed and how the user will benefit.  Then, the coder can talk about what lines were added/changed.  Lastly, how the code works and how the coder knows it's working is explained last.
Goal: All code that lives on production be reviewed by at least one other team member.
##On Giving FeedbackI'd basically like us to get to the point where we can all comfortably bring up questions about each other's code.  Maybe some of these suggestions will help us get there:
* We're all adults
* Avoid knee-jerk reactions
* At the same time, don't let something fester
* Don't make assumptions about unknown items
* Don't be mean-spiritied
* Don't be overly diplomatic 
* Be positive
* Be specific# ##Behavior/Test Driven Development
Test-Driven Development (TDD) can remove the guess work and provide greater confidence in code coverage.
We should take a really close look at test suites available to incorporate in our project. Selenium and PhantomJS/CasperJS are two examples.  SimpleTest is a tool that the Drupal project uses.  
I'd like us to take a look at [Behat](http://docs.behat.org/en/v2.5/) too, which is shorthand for *behavioral-driven development*.  From the Behat site:
> It’s the idea that you start by writing human-readable sentences that describe > a feature of your application and how it should work, and only then implement this > behavior in software.